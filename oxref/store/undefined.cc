#include "store.ih"

    // 00000000         *UND*  00000000 std::ios_base::Init::Init()
    //                                  ---------------------------

void Store::undefined(std::string const &symbol)
{
    // find 'symbol' in d_xrefVector. 
    // If not yet available add it to xrefData.
    // Determine the symbol's index in xrefData
    // The function currently handled has a currentIdx and from that index
    // the current *UND* symbol is called.
                
    auto iter = find_if(
                    d_xrefVector.begin(), d_xrefVector.end(),
                    [&](XrefData const &xrefData)
                    {
                        return xrefData.hasSymbol(symbol);
                    }
                );

    size_t index = iter - d_xrefVector.begin();   // index of this symbol

    if (iter == d_xrefVector.end())               // symbol not yet defined
    {
        index = d_xrefVector.size();
        d_xrefVector.push_back(XrefData{ symbol });   // define the symbol
    }

    d_xrefVector[index].calledFrom(d_currentIdx);
}







