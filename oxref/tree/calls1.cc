#include "tree.ih"

size_t Tree::calls(size_t symIdx, size_t from)
{
    auto iter = find_if(d_xrefVector.begin() + from, d_xrefVector.end(), 
        [&](XrefData const &data)
        {
            return calls(symIdx, data);
        }
    );

    return iter == d_xrefVector.end() ? 
                        d_xrefVector.size() 
                    :
                        iter - d_xrefVector.begin();
}
