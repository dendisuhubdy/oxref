#include "tree.ih"

void Tree::print(size_t symIdx, size_t level)
{
    string const &name = d_xrefVector[symIdx].symbol(); // the symbol name
    cout << name;

    for (size_t idx = 0; idx != level; ++idx)
    {
        if (d_info[idx].symIdx == symIdx)       // recursive call?
        {
            cout << " ==> " << idx << '\n';     // show the recursion level
            return;
        }
    }

    cout << '\n';    

    size_t callIdx = calls(symIdx, 0);          // idx of 1st called entity
    if (callIdx == d_xrefVector.size())         // nothing called from here
        return;

    d_info.resize(level + 1);                   // room for the next level
    d_info[level].symIdx = symIdx;              // store this symbol's idx

    size_t nextLevel = level + 1;

    while (true)
    {
        size_t nextIdx = calls(symIdx, callIdx + 1);    // anything else
                                                        // called? 

                                                // callIdx refers to the last
                                                // called entity ?
        d_info[level].last =  nextIdx == d_xrefVector.size();

//        cout << "   " << d_xrefVector[callIdx].symbol() << '\n';
        indent(level);
        print(callIdx, nextLevel);

        if (nextIdx == d_xrefVector.size())
            break;

        callIdx = nextIdx;
    }



///////////////////////////////////////////////////////////////////
//    auto iter
//    size_t size = d_idMap.callSize(name);
//                  d_xrefVector[symIdx].
//
//    d_info.resize(level + 1);
//
//    d_info[level] = Data{ size, 0, name };
//
//    size_t nextLevel = level + 1;   // next recursion
//
//    for (size_t elem = 0; elem != size; ++elem)
//    {
//        ++d_info[level].current;
//        indent(level);
//        print(d_idMap.element(elem, name), nextLevel);
//    }
}
