#ifndef INCLUDED_TREE_
#define INCLUDED_TREE_

#include <vector>
#include <string>

#include "../xrefdata/xrefdata.h"

class Tree
{
    struct Data;

    std::vector<Data> d_info;
    XrefVector const &d_xrefVector;

    public:
        Tree(XrefVector const &xrefVector);

        void print(size_t idx, size_t level = 0);   // idx in d_xrefVector
                                                    // of the symbol to print

    private:
        void indent(size_t level) const;

        size_t calls(size_t symIdx, size_t from);               // 1.cc
        static bool calls(size_t symIdx, XrefData const &data); // 2.cc

};

struct Tree::Data
{
    bool last;          // reached the last entity called by this function 
    size_t symIdx;      // xrefVector index holding the info about the
                        // called entity
};

inline Tree::Tree(XrefVector const &xrefVector)
:
    d_xrefVector(xrefVector)
{}
        
#endif
