#include "storage.ih"

void Storage::storeReplacement(string const &label, string const &spec)
{
    int separator;
    if (
        spec.length() < 3 
        or 
        count(spec.begin(), spec.end(), separator = spec[0]) != 3
        or
        spec.back() != separator
    )
        throw Exception{} << label << ": invalid pattern `" << spec << '\'';

    size_t pos = spec.find(separator, 1);           // find the middle sep.

    d_replacement.push_back(                        // store the pattern
                            { spec.substr(1,       pos - 1), 
                              spec.substr(pos + 1, spec.length() - pos - 2 ) }
                        );
}
