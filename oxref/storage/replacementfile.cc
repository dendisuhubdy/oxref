#include "storage.ih"

void Storage::replacementFile(string const &fname)
{
    ifstream in{ Exception::factory<ifstream>(fname) };

    string label{ "pattern file `" + fname + '\'' };
    string line;
    while (getline(in, line))
    {
        line = String::trim(line);
        if (line.empty() or line.find("//") == 0)   // empty or comment
            continue;

        storeReplacement(label, line);
    }
}
