#include "storage.ih"

    // replace contents of lines by replacement text. Format:
    //  XencounteredXreplaceX
void Storage::replacements(string &line) const
{
    for (auto const &replace: d_replacement)
    {
        size_t srcLen = replace.first.length();
        size_t dstLen = replace.second.length();

        size_t pos = 0;

        while (true)
        {
            pos = line.find(replace.first, pos);
            if (pos == string::npos)            // no more matches
                break;

            line.replace(pos, srcLen, replace.second);
            pos += dstLen;
        }
    }
}
