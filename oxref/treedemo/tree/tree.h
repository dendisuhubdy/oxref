#ifndef INCLUDED_TREE_
#define INCLUDED_TREE_

#include <vector>
#include <string>

class IDmap;

class Tree
{
    struct Data;

    std::vector<Data> d_info;
    IDmap const &d_idMap;

    public:
        Tree(IDmap const &idMap);

        void print(std::string const &name, size_t level = 0);

    private:
        void indent(size_t level) const;
};

struct Tree::Data
{
    size_t callSize;
    size_t current;
    std::string name;
};

inline Tree::Tree(IDmap const &idMap)
:
    d_idMap(idMap)
{}
        
#endif
