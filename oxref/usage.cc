//                     usage.cc

#include "main.ih"

namespace {

char const usageText[] = R"((
Where:
   [options] - optional arguments (short options between parentheses):

      --arg (-a) ...
              count - display function argument counts only
              first - only display the first word of the parameter types
              <len> - restrict parameter types to the first <len> characters
      --full-symbol (-f)      - display the full symbol names (e.g.,
                                including class name prefixes)
      --help (-h)             - provide this help
      --no-xref (-X)          - don't generate the cross-reference listing
      --objdump command       - the command calling the object dump program
                                (see the man-page for details)
      --object-files (-o)     - display the object file names
      --replace (-R) spec     - spec is XfirstXsecondX. 'first' found in 
                                objdump's output is replaced by 'second'; X is
                                a unique separator character.
                                (multiple -R options may be specified)
      --replace-file (-r) fname - fname contains 'spec' replacement
                                specifications as used with the --replace
                                options (empty lines and lines beginning with
                                // are skipped)  
      --select name           - only display the cross-reference of `name',
                                where `name' is the (initial substring of an)
                                entity's name.
      --select-pattern regex  - only display the cross-reference of `regex',
                                where `regex' is a regular expression
                                against which (full) symbols are matched
      --source-files (-s)     - display the source file names
      --tree (-t) symbol      - display the call-tree for symbol
      --xref-source-files (-x)- display the source file names in the 
                                cross-reference lists
      --version (-v)          - show version information and terminate

   args   - non-stripped .o or libraries whose symbols must be
            cross-referenced.

))";

}   // namespace
void usage(std::string const &progname)
{
    cout << '\n' <<
    progname << " by " << Icmbuild::author << '\n' <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << " [options] args" <<
    usageText;
}
